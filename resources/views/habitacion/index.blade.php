@extends('layouts.base')
@section('body')
	<section class="content-header">
		  <h1>
			Habitaciones
			<small>Gestion</small>
		  </h1>
	</section>

	<section class="content">
	<div class="row">
		<div class="col-lg-8">
		   <div class="panel panel-success">
	
	<div class="panel-body">
		{!! Form::btnregistrar('habitacion/create') !!}
		<table class="table table-striped table-hover" >
		<tr>
			<th>#</th>
			<th>NUMERO</th>
			<th>TIPO</th>
			<th>COSTO</th>
			<th>ESTADO</th>
			<th>DISPONIBILIDAD</th>
			<th>ACCIONES</th>
		</tr>
		@foreach ($items as $index => $item)
		<tr>
			<td>{{ $item->id }}</td>
			<td>{{ $item->number }}</td>
			<td>{{ $item->type_description() }}</td>
			<td>{{ $item->cost }}</td>
			<td>{{ $item->state_description() }}</td>			
			<td>{{ $item->available_description() }}</td>
			<td>

			<div class="col-md-3 ">
			{!! Form::btnmostrar('habitacion',$item->id) !!}
			</div>
			@if($item->available=='s')
			<div class="col-md-3 ">
			{!! Form::btneditar('habitacion',$item->id) !!}
			</div>
			<div class="col-md-3 ">
			{!! Form::btneliminar('habitacion/',$item->id) !!}
			</div>
			@endif
			</td>
		</tr>
		@endforeach
	 </table>
	</div>
	
	</div>
		</div>
	</div>
	</section>

@stop