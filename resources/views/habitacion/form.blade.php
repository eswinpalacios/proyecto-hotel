@extends('layouts.base')
@section('body')

<section class="content-header">
    <h1>
        Habitaciones
        <small>Registrar</small>
    </h1>
</section>

<section class="content">
<div class="row">
    <div class="col-lg-3">
	   <div class="panel panel-success">

            <div class="panel-body">
            	@foreach ($errors->all() as $error)
                    <p class="error">{{ $error }}</p>
                @endforeach

                @if($action == "register")
                    <form action=" {{ url('habitacion') }} " method="post" >
                @elseif($action == "update")
                    <form action=" {{ url('habitacion').'/'.$item->id }} " method="post" >
                   	<input type="hidden" name="_method" value="PUT" >
                @endif
                
                <label>Tipo :</label>
                <select id="type" name="type" class="form-control" @if($action=="show") disabled @endif >
                    <option value="s">Simple</option>
                    <option value="m">Matrimonial</option>        
                    <option value="d">Doble</option>
                </select>
                <br/>

                <label>Numero :</label>
                <input type="text" name="number" class="form-control" value="{{ isset($item->number) ? $item->number : '' }}" @if($action=="show") disabled @endif>
                <br/>

                <label>Costo :</label>
                <input type="text" name="cost" class="form-control" value="{{ isset($item->cost) ? $item->cost : '' }}" @if($action=="show") disabled @endif>
                <br/>

                @if($action!="show")
                    {!! Form::btnaceptar($action) !!}
                @endif

                {!! Form::btncancelar('habitacion') !!}
                {!! Form::token() !!}
                </form>

            </div>
        </div>
    </div>
</div>
</section>
@stop

