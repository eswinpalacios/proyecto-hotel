@extends('layouts.base')
@section('body')

<section class="content-header">
    <h1>
        Reserva de Habitacion
    </h1>
</section>

<section class="content">
<div class="row">
    <div class="col-lg-3">
	   <div class="panel panel-success">
            <p>La reserva se realizó correctamente.</p>

            <p>CLIENTE: {{ $client->name }} {{ $client->last_name }} {{ $client->dni }}</p>
            <p>HABITACION: {{ $room->type_description() }}</p>
            <p>COSTO: s/.{{ $room->cost }}</p>
            <p>NUMERO: {{ $room->number }}</p>
            <p>FECHA: {{ $reservation->date }}</p>
        </div>
    </div>


</div>
</section>
@stop

