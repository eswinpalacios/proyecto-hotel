@extends('layouts.base')
@section('body')
	<section class="content-header">
		  <h1>
			Reserva de Habitaciones
		  </h1>
	</section>

	<section class="content">
	<div class="row">
		<div class="col-lg-8">
		   <div class="panel panel-success">
	
	<div class="panel-body">
		
		<table class="table table-striped table-hover" >
		<tr>
			<th>#</th>
			<th>TIPO</th>
			<th>NUMERO</th>			
			<th>COSTO</th>
			<th></th>
		</tr>
		@foreach ($items as $index => $item)
		<tr>
			<td>{{ $item->id }}</td>
			<td>{{ $item->type_description() }}</td>
			<td>{{ $item->number }}</td>			
			<td>s/. {{ $item->cost }}</td>
			<td>		        
			<a href="{{ url('reserva/'.$item->id) }}" class="btn btn-success"><i class="fa fa-hand-o-right"></i> Reservar</a>
			</td>
		</tr>
		@endforeach
	 </table>
	</div>
	
	</div>
		</div>
	</div>
	</section>

@stop