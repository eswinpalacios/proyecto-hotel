@extends('layouts.base')
@section('body')

<section class="content-header">
    <h1>
        Reserva de Habitacion
    </h1>
</section>

<section class="content">
<div class="row">
    <div class="col-lg-3">
	   <div class="panel panel-success">
            <p>HABITACION</p>
            <div class="panel-body">

                @if($action == "register")
                    <form action="#" method="post" >
                @endif
                
                <label>Tipo :</label>
                <select id="type" name="type" class="form-control" disabled >
                    <option value="s">Simple</option>
                    <option value="m">Matrimonial</option>        
                    <option value="d">Doble</option>
                </select>
                <br/>

                <label>Numero :</label>
                <input type="text" name="number" class="form-control" value="{{ isset($item->number) ? $item->number : '' }}" disabled>
                <br/>

                <label>Costo :</label>
                <input type="text" name="cost" class="form-control" value="{{ isset($item->cost) ? $item->cost : '' }}" disabled>
                <br/>

                </form>

            </div>
        </div>
    </div>

        <div class="col-lg-3">
       <div class="panel panel-success">
            <p>CLIENTE</p>
            <div class="panel-body">

                @if($action == "register")
                    <form action=" {{ url('reserva/'.$item->id) }} " method="post" >
                @elseif($action == "show")
                    <form action="#" method="post" >          
                @endif

                <label>NOMBRES :</label>
                <input type="text" name="name" class="form-control" value="{{ isset($item->name) ? $item->name : '' }}" @if($action=="show") disabled @endif>
                <br/>

                <label>APELLIDOS :</label>
                <input type="text" name="last_name" class="form-control" value="{{ isset($item->last_name) ? $item->last_name : '' }}" @if($action=="show") disabled @endif>
                <br/>

                <label>DNI :</label>
                <input type="text" name="dni" class="form-control" value="{{ isset($item->dni) ? $item->dni : '' }}" @if($action=="show") disabled @endif>
                <br/>

                <label>FECHA :</label>
                <input type="text" name="date" class="form-control" value="{{ isset($item->date) ? $item->date : '' }}" @if($action=="show") disabled @endif>
                <br/>
                

                @if($action!="show")
                    {!! Form::btnaceptar($action) !!}
                @endif

                {!! Form::btncancelar('reserva') !!}
                {!! Form::token() !!}
                </form>

            </div>
        </div>
    </div>

</div>
</section>
@stop

