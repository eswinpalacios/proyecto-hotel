<!DOCTYPE html>
<html lang="es">  
  <head>
  	    <meta charset="UTF-8">
  	    <title>Hotel</title>
  	    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  	    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />
  	    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" />
  	    <link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet" />
  	    <link href="{{ asset('css/skins/_all-skins.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('img/link.ico') }}" type="image/x-icon" rel="icon" />
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body class="skin-blue sidebar-mini">

<div class="wrapper">

  	<header class="main-header">
        <!-- Logo -->
        <a href="{{ url('start') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>H</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Hotel</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          
        </nav>
    </header>

  	<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          @if(session()->has('id'))
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ asset('img/user/'.session('img').'') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>{{ session('name') }}</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          @endif
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">OPCIONES PUBLICAS</li>
            <li><a href="{{ url('reserva') }}"><i class="fa fa-search"></i> <span>Reservar</span></a></li>            
            <li class="header">OPCIONES INTERNAS</li>
            <li><a href="{{ url('habitacion') }}"><i class="fa fa-building"></i> <span>Gestion Habitaciones</span></a></li>
            <li><a href="{{ url('estadia') }}"><i class="fa fa-building"></i> <span>Confirmar Reserva</span></a></li>
            <li><a href="{{ url('clientes') }}"><i class="fa fa-building"></i> <span>Clientes Hospedados</span></a></li></a></li>
            <li><a href="{{ url('clientes/historial') }}"><i class="fa fa-building"></i> <span>Clientes Historial</span></a></li></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

  	<div class="content-wrapper">  		
  		@yield('body')      	
  	</div>
  	
    <footer class="main-footer"></footer>
	
  	<div class='control-sidebar-bg'></div>
</div>

    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>    
    <script src="{{ asset('js/app.min.js') }}"></script>
    @yield('js')
  </body>
</html>