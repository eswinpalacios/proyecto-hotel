@extends('layouts.base')
@section('body')
	<section class="content-header">
		  <h1>
			Reservas por Confirmar
		  </h1>
	</section>

	<section class="content">
	<div class="row">
		<div class="col-lg-8">
		   <div class="panel panel-success">
	
	<div class="panel-body">
		
		<table class="table table-striped table-hover" >
		<tr>
			<th>#</th>
			<th>TIPO</th>
			<th>NUMERO</th>			
			<th>COSTO</th>
			<th>CLIENTE</th>
			<th>DNI</th>
			<th></th>
		</tr>
		@foreach ($items as $index => $item)
		<tr>
			<td>{{ $item->room->id }}</td>
			<td>{{ $item->room->type_description() }}</td>
			<td>{{ $item->room->number }}</td>			
			<td>s/. {{ $item->room->cost }}</td>
			<td>{{ $item->client->name }} {{ $item->client->last_name }}</td>
			<td>{{ $item->client->dni }}</td>
			<td>		        
			<a href="{{ url('estadia/'.$item->id) }}" class="btn btn-success"><i class="fa fa-hand-o-right"></i> Confirmar</a>
			<a href="{{ url('estadia/'.$item->id.'/anular') }}" class="btn btn-danger"><i class="fa fa-close"></i> Anular</a>
			</td>
		</tr>
		@endforeach
	 </table>
	</div>
	
	</div>
		</div>
	</div>
	</section>

@stop