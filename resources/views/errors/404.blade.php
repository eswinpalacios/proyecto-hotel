@extends('layouts.base')
@section('body')
		
<section class="content-header">
	<h1>
		Error
		<small>Pagina No Encontrada</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('start') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	</ol>
</section>

<section class="content">
	<div class="row">
	<div class="col-lg-5">
		<div class="panel panel-primary">
			<div class="panel-body">
				<img src="{{ asset('img/error.png') }}">
				<p>Pagina no encontrada.</p>
			</div>
		</div>
	</div>
</section>

@stop