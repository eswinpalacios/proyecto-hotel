@extends('layouts.base')
@section('body')
	<section class="content-header">
		  <h1>
			Clientes Hospedados
		  </h1>
	</section>

	<section class="content">
	<div class="row">
		<div class="col-lg-8">
		   <div class="panel panel-success">
	
	<div class="panel-body">
		
		<table class="table table-striped table-hover" >
		<tr>
			<th>#</th>
			<th>TIPO</th>
			<th>NUMERO</th>			
			<th>COSTO</th>
			<th>CLIENTE</th>
			<th>DNI</th>
			<th></th>
		</tr>
		@foreach ($items as $index => $item)
		<tr>
			<td>{{ $item->room->id }}</td>
			<td>{{ $item->room->type_description() }}</td>
			<td>{{ $item->room->number }}</td>			
			<td>s/. {{ $item->room->cost }}</td>
			<td>{{ $item->client->name }} {{ $item->client->last_name }}</td>
			<td>{{ $item->client->dni }}</td>
			<td>		        
			<a href="{{ url('clientes/'.$item->id.'/finalizar') }}" class="btn btn-success"><i class="fa fa-check"></i> Finalizar Hospedaje</a>
			</td>
		</tr>
		@endforeach
	 </table>
	</div>
	
	</div>
		</div>
	</div>
	</section>

@stop