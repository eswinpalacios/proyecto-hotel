@extends('layouts.base')
@section('body')
	<section class="content-header">
		  <h1>
			Reservas por Confirmar
		  </h1>
	</section>

	<section class="content">
	<div class="row">
		<div class="col-lg-8">
		   <div class="panel panel-success">
	
	<div class="panel-body">
		
		<table class="table table-striped table-hover" >
		<tr>
			<th>#</th>
			<th>TIPO</th>
			<th>NUMERO</th>			
			<th>COSTO</th>
			<th>FECHA</th>
			<th>CLIENTE</th>
			<th>DNI</th>
		</tr>
		@foreach ($items as $index => $item)
		<tr>
			<td>{{ $item->room->id }}</td>
			<td>{{ $item->room->type_description() }}</td>
			<td>{{ $item->room->number }}</td>
			<td>s/. {{ $item->room->cost }}</td>
			<td>{{ substr($item->date,0,10) }}</td>
			<td>{{ $item->client->name }} {{ $item->client->last_name }}</td>
			<td>{{ $item->client->dni }}</td>
		</tr>
		@endforeach
	 </table>
	</div>
	
	</div>
		</div>
	</div>
	</section>

@stop