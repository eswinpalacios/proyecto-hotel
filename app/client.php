<?php 

namespace App; //namespace obligatorio

use Illuminate\Database\Eloquent\Model; //para instanciar la clase

class Client extends Model {
	protected $table = 'client';
}