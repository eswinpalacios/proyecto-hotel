<?php 

namespace App\Http\Controllers;

use App\room;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = Room::orderBy('number')->get();
        $data = array('items' => $items);
        return view('habitacion.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data = array('action' => 'register');
        return view('habitacion.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $cat = new Room;
        $cat->number = $request->input('number');
        $cat->cost = $request->input('cost');
        $cat->type = $request->input('type');
        $cat->state = 'a';
        $cat->available = 's';
        $cat->save();

        return redirect('habitacion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {        
        $item = Room::find($id);
        if($item == null)
            return redirect('habitacion');

        $data = array('action' => 'show', 'item' => $item);
        return view('habitacion.form', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $item = Room::find($id);
        if($item == null)
            return redirect('habitacion');

        $data = array('item' => $item, 'action' => 'update');
        return view('habitacion.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $item = Room::find($id);
         if($item == null)
            return redirect('habitacion');

        $item->number = $request->input('number');
        $item->cost = $request->input('cost');
        $item->type = $request->input('type');
        $item->save();
        
        return redirect('habitacion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $item = Room::find($id);
        if($item == null)
            return redirect('habitacion');

        Room::destroy($id);
        return redirect('habitacion');
    }
}
