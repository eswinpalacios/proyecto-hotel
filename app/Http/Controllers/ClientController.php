<?php 

namespace App\Http\Controllers;

use App\reservation;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = Reservation::where('state','=','a')->get();
        $data = array('items' => $items);
        return view('clientes.index', $data);
    }
    
}
