<?php 

namespace App\Http\Controllers;

use App\room;
use App\client;
use App\reservation;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = Room::where('available','=','s')->where('state','=','a')->orderBy('number')->get();
        $data = array('items' => $items);
        return view('reserva.index', $data);
    }

    public function reservation($id)
    {
        $item = Room::find($id);
        if($item == null)
            return redirect('reserva');

        $data = array('action' => 'register', 'item' => $item);
        return view('reserva.form', $data);
    }

    public function reservation_save($id, Request $request)
    {
        $item = Room::find($id);
        if($item == null)
            return redirect('reserva');

        $item->available = 'n';
        $item->save();

        $client = Client::where('dni','=',$request->input('dni'))->first();
        
        if($client==null){
            $client = new Client;
            $client->dni = $request->input('dni');
            $client->name = $request->input('name');
            $client->last_name = $request->input('last_name');
            $client->save();
        }

        $reservation = new Reservation;
        $reservation->date = $request->input('date');
        $reservation->room_id = $id;
        $reservation->client_id = $client->id;
        $reservation->state = 'p';
        $reservation->save();

        $data = array('client' => $client, 'reservation' => $reservation, 'room' => $item);

        return view('reserva.confirm', $data);
    }

    public function reservation_pendient()
    {
        $items = Reservation::where('state','=','p')->get();
        $data = array('items' => $items);
        return view('estadia.index', $data);
    }

    public function reservation_confirm($id)
    {
        $reservation = Reservation::find($id);
        $reservation->state='a';
        $reservation->save();

        return redirect("estadia");
    }

    public function reservation_cancel($id)
    {
        $reservation = Reservation::find($id);
        $reservation->state='c';
        $reservation->save();

        $room = Room::find($reservation->room_id);
        $room->available = 's';
        $room->save();

        return redirect("estadia");
    }

    public function reservation_finish($id)
    {
        $reservation = Reservation::find($id);
        $reservation->state='f';
        $reservation->save();

        $room = Room::find($reservation->room_id);
        $room->available = 's';
        $room->save();

        return redirect('clientes');
    }

    public function historial()
    {
        $items = Reservation::where('state','=','f')->get();
        $data = array('items' => $items);
        return view('clientes.historial',$data);
    }
}
