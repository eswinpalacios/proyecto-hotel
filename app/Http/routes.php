<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//$router->controller('user', 'UserController');

//EXTERNO

$router->get('/', 'ReservationController@index');
$router->get('clientes', 'ClientController@index');
$router->get('clientes/historial', 'ReservationController@historial');
$router->get('clientes/{id}/finalizar', 'ReservationController@reservation_finish');
$router->get('estadia', 'ReservationController@reservation_pendient');
$router->get('estadia/{id}', 'ReservationController@reservation_confirm');
$router->get('estadia/{id}/anular', 'ReservationController@reservation_cancel');
$router->get('reserva', 'ReservationController@index');
$router->get('reserva/{id}', 'ReservationController@reservation');
$router->post('reserva/{id}', 'ReservationController@reservation_save');

$router->resource('habitacion','RoomController');