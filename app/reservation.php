<?php 	

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model {
	protected $table = 'reservation';

	public function room()
	{		
		return $this->belongsTo('App\room');
	}

	public function client()
	{
		return $this->belongsTo('App\client');	
	}

}