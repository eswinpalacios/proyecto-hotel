<?php

namespace App\Providers;

use Illuminate\Html\HtmlServiceProvider; //class
use Log, Html, Request; //class

class MacroServiceProvider extends HtmlServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        parent::register();
            require base_path().'/app/macros.php';
    }
}
