<?php 

use Illuminate\Html\HtmlServiceProvider;

Form::macro('btnregistrar', function ($recurso){
	$a = '<a href="'.$recurso.'">';
	$a.= Form::button('<span class="glyphicon glyphicon-floppy-open"></span> registrar', 
				array( 'class' => 'btn btn-primary') );
	$a.= '</a>';
	return $a;
});

Form::macro('btnaceptar', function($accion)
{   
    return '<button class="btn btn-primary" type="submit" 
    ><span class="glyphicon glyphicon-floppy-open"></span> '.$accion.'</button>';
});

Form::macro('btncancelar', function($recurso)
{   
    $a = '<a href="'.url($recurso).' ">';
    $a.= Form::button('<span class="glyphicon glyphicon-remove"></span> Cancelar', 
				array( 'class' => 'btn btn-danger') );
    $a.= '</a>';
    return $a;
});

Form::macro('btneditar', function($recurso, $id)
{   
    return '<a href="'.$recurso.'/'.$id.'/edit"><button class="btn btn-primary"
	><span class="glyphicon glyphicon-floppy-open"></span> editar</button></a>';
});

Form::macro('btnmostrar', function($recurso, $id)
{   
    return '<a href="'.$recurso.'/'.$id.'"><button class="btn btn-primary"
	><span class="glyphicon glyphicon-floppy-open"></span> ver</button></a>';
});

Form::macro('btnregresar', function($direccion)
{   
    return '<a href="'.$direccion.'"><button class="btn btn-primary"
	><span class="glyphicon glyphicon-floppy-open"></span> regresar</button></a>';
});

Form::macro('btneliminar', function($recurso, $id)
{
	$a = Form::open(array('url' =>  $recurso. $id));
	$a.= Form::hidden('_method', 'DELETE');
	$a.= Html::decode(Form::button(
					'<span class="glyphicon glyphicon-remove"></span> eliminar', 
					array('class' => 'btn btn-danger ', 'type'=> 'submit'))) ;
	$a.= Form::close();
	return $a;
});	

?>