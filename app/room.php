<?php 	

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model {
	protected $table = 'room';

	function type_description()
	{
		switch ($this->type) {
			case 's': return 'SIMPLE'; break;
			case 'd': return 'DOBLE'; break;
			case 'm': return 'MATRIMONIAL'; break;
		}
		return;
	}

	function state_description()
	{
		if($this->state=="a")
			return "habilitada";
		else
			return "deshabilitada";
	}

	function available_description()
	{
		if($this->available=="s")
			return "disponible";
		else
			return "ocupada";
	}
}